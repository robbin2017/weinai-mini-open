# weinai-mini-open

#### 介绍
这是一个记录喂奶的小程序，可以微信搜索小程序【喂奶速记】查看运行中的样子


#### 安装教程

里面涉及到小程序AppId和后端接口权限的配置做了隐藏，修改的地方包括：
1. /utils/config.js
```
const appid = '【TODO 这里是小程序的appid】';
const requestKey = '【TODO 这里是和后端约定的一个key】';
// 服务器域名
const baseUrl = '【TODO 这里是域名】';

```
2. project.config.json
```
"appid": "【TODO 这里是小程序的appid】",
```

#### 使用说明

1.  本代码仅可用于交流学习

