const app = getApp()
const config = require('../../utils/config.js');
const picker = require('../../utils/picker.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    statusBarHeight: app.globalData.statusBarHeight,
    recordId:0,
    optionActive:3,
    selectColor3: 'color: #FF9DCA;',
    selectColor4: '',
    selectColor5: '',
    selectedDate: '今天',
    selectedDateEnd: '今天',
    dateRange:['今天','昨天'],
    selectedTime: '00:00',
    selectedTimeEnd: '00:00',
    remark:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let recordId = options.recordId;
    if (recordId != undefined) {
      //查询记录
      this.setData({
        recordId: recordId
      })
      let that = this
      let param = { recordId: recordId}
      config.postApi(config.getRecord, param).then((res) => {
        that.setData({
          optionActive: res.selType,
          quantity: res.quantity,
          selectedDate: res.beginTimeDate,
          selectedTime: res.beginTimeTime,
          selectedDateEnd: res.endTimeDate,
          selectedTimeEnd: res.endTimeTime,
          remark: res.remark,
        })
        if (res.selType == 5) {
          that.setData({
            selectColor3: '',
            selectColor4: '',
            selectColor5: 'color: #FF9DCA;',
          })
        } else if (res.selType == 4) {
          that.setData({
            selectColor3: '',
            selectColor4: 'color: #FF9DCA;',
            selectColor5: '',
          })
        } else {
          that.setData({
            selectColor3: 'color: #FF9DCA;',
            selectColor4: '',
            selectColor5: '',
          })
        }
      }).catch((res) => {
        console.log(res)
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    let dateRange = picker.pickerDate();
    let curtime = new Date();
    let selectedTime = '';
    if (curtime.getHours() < 10) {
      selectedTime += '0'
    }
    selectedTime += curtime.getHours() + ':';
    if (curtime.getMinutes()<10) {
      selectedTime+='0'
    }
    selectedTime += curtime.getMinutes();
    this.setData({
      dateRange: dateRange,
      selectedTime: selectedTime,
      selectedTimeEnd: selectedTime,
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  goBack: function () {
    wx.navigateBack({})
  },
  selectOption: function (e) {
    let option = e.currentTarget.dataset.option;
    if (option==5) {
      this.setData({
        optionActive: 5,
        selectColor3: '',
        selectColor4: '',
        selectColor5: 'color: #FF9DCA;',
      })
    } else if (option == 4) {
      this.setData({
        optionActive: 4,
        selectColor3: '',
        selectColor4: 'color: #FF9DCA;',
        selectColor5: '',
      })
    } else {
      this.setData({
        optionActive: 3,
        selectColor3: 'color: #FF9DCA;',
        selectColor4: '',
        selectColor5: '',
      })
    }
  }, 
  listenQuantity: function (e) {
    var val = e.detail.value;
    this.setData({
      quantity: val
    });
  },
  listenRemark: function (e) {
    this.setData({
      remark: e.detail.value
    });
  },
  addNiaobu: function () {
    let param = {
      recordId: this.data.recordId,
      recordType: 2,
      selType: this.data.optionActive,
      beginTime: this.data.selectedDate + ' ' + this.data.selectedTime,
      remark: this.data.remark,
    }
    config.postApi(config.addRecord, param).then((res) => {
      wx.showToast({
        title: "记录成功",
        icon: 'none',
        image: '',
        duration: 1500,
        mask: false,
      });
      setTimeout(function () {
        wx.navigateBack({})
      }, 1000)
    }).catch((res) => {
      console.log(res)
    })
    
  },
  bindDateChange(e) {
    this.setData({
      selectedDate: this.data.dateRange[e.detail.value]
    })
  },
  bindTimeChange(e) {
    this.setData({
      selectedTime: e.detail.value
    })
  },
  bindDateChangeEnd(e) {
    this.setData({
      selectedDateEnd: this.data.dateRange[e.detail.value]
    })
  },
  bindTimeChangeEnd(e) {
    this.setData({
      selectedTimeEnd: e.detail.value
    })
  },

})