const app = getApp()
const config = require('../../utils/config.js');
const auth = require('../../utils/auth.js');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    statusBarHeight: app.globalData.statusBarHeight,
    isEmpty:false,
    showModal: false,
    dateBegin: '',
    dateEnd: '',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getlogs('')
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  getlogs: function (msg) {
    let that = this;
    let param = {
      beginTime: this.data.dateBegin,
      endTime: this.data.dateEnd,
    }
    config.postApi(config.getRecordList, param).then((res) => {
      let dayList = res.dayList
      that.setData({
        dayList: dayList,
        isEmpty: res.isEmpty,
      })
      if (msg != '') {
        wx.showToast({
          title: msg,
          icon: 'none',
          image: '',
          duration: 1500,
          mask: false,
        });
      }
    }).catch((res) => {
      console.log(res)
    })
  },
  gotoDay: function (event) {
    let day = event.currentTarget.dataset.day;
    let dayStr = event.currentTarget.dataset.daystr;
    wx.navigateTo({
      url: '/pages/oneday/oneday?day=' + day+'&dayStr='+dayStr
    })
  },
  showModalFuc: function () {
    let dateBegin = this.data.dateBegin;
    if (dateBegin == '') {
      var nowDate = new Date();//获取系统当前时间
      var nowMonth = nowDate.getMonth() + 1;
      var today = nowDate.getFullYear() + "-" + nowMonth + "-" + nowDate.getDate();
      this.setData({
        dateBegin: today,
        dateEnd: today,
      })
    }
    this.setData({
      showModal: true,
    })
  },
  modalClose: function () {
    this.setData({
      showModal: false,
    })
  },
  bindPickerBeginDateChange: function (e) {
    this.setData({
      dateBegin: e.detail.value
    });
  },
  bindPickerEndDateChange: function (e) {
    this.setData({
      dateEnd: e.detail.value
    });
  },
  modalConfirm: function () {
    this.getlogs("查询成功")
    this.setData({
      showModal: false,
    })
  }
})
