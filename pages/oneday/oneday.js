// pages/today/today.js
const app = getApp()
const config = require('../../utils/config.js');
const auth = require('../../utils/auth.js');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    statusBarHeight: app.globalData.statusBarHeight,
    recordList:[],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let day = options.day;
    let dayStr = options.dayStr;
    if (day != undefined) {
      this.setData({
        day: day,
        dayStr: dayStr,
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getTodayRecordList();
  },

  getTodayRecordList: function () {
    let that = this;
    config.postApi(config.getOneDayRecord, {day: that.data.day}).then((res) => {
      let list = res.list
      that.setData({
        recordList: list,
      })
    }).catch((res) => {
      console.log(res)
    })
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  getUserInfo: function (e) {
    let that = this
    console.log(e)
    let userInfo = e.detail.userInfo
    userInfo.isLogin = true
    wx.setStorageSync('userInfo', userInfo);
    this.setData({
      userInfo: userInfo,
    })
    wx.login({
      success: res => {
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
        let param = {
          sessionId: that.data.sessionId,
          code: res.code,
        }
        console.log('微信登录请求：'+param)
        config.postApi(config.wxLogin, param).then((res) => {
          let newSessionId = res.sessionId
          wx.setStorageSync('sessionId', newSessionId);
          that.setData({
            sessionId: newSessionId,
          })
          //保存用户的信息
          config.postApi(config.addUserWxInfo, userInfo).then((res) => {
            //保存成功
            this.getTodayRecordList();
          }).catch((res) => {
            console.log(res)
          })
        }).catch((res) => {
          console.log(res)
        })
      }
    })
    
  },
  addWeinai: function () {
    wx.navigateTo({
      url: '/pages/weinai/weinai'
    })
  },
  addNiaobu: function () {
    wx.navigateTo({
      url: '/pages/niaobu/niaobu'
    })
  },
  addShuijiao: function () {
    wx.navigateTo({
      url: '/pages/shuijiao/shuijiao'
    })
  },
  addFushi: function () {
    wx.navigateTo({
      url: '/pages/fushi/fushi'
    })
  },
  addXinai: function () {
    wx.navigateTo({
      url: '/pages/xinai/xinai'
    })
  },
  gotoRecord: function (e) {
    let id = e.currentTarget.dataset.id;
    let recordType = e.currentTarget.dataset.recordtype;
    if (recordType==1) {
      wx.navigateTo({
        url: '/pages/weinai/weinai?recordId='+id
      })
    } else if (recordType == 2) {
      wx.navigateTo({
        url: '/pages/niaobu/niaobu?recordId=' + id
      })
    } else if (recordType == 3) {
      wx.navigateTo({
        url: '/pages/shuijiao/shuijiao?recordId=' + id
      })
    } else if (recordType == 4) {
      wx.navigateTo({
        url: '/pages/fushi/fushi?recordId=' + id
      })
    } else if (recordType == 5) {
      wx.navigateTo({
        url: '/pages/xinai/xinai?recordId=' + id
      })
    }
  },
  delRecord: function (e) {
    let that = this;
    let id = e.currentTarget.dataset.id;
    let recordType = e.currentTarget.dataset.recordtype;
    wx.showModal({
      title: '',
      content: '是否删除此记录?',
      showCancel: true,
      confirmText: '删除',
      confirmColor: '#f5222d',
      success(res) {
        if (res.confirm) {
          config.postApi(config.delRecord, {recordId:id}).then((res) => {
            //删除成功
            wx.showToast({
              title: "删除成功",
              icon: 'none',
              image: '',
              duration: 1500,
              mask: false,
            });
            that.getTodayRecordList();
          }).catch((res) => {
            console.log(res)
          })
        }
      }
    })
  },
  goBack: function () {
    wx.navigateBack({})
  },

})