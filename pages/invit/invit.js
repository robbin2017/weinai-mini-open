// pages/today/today.js
const app = getApp()
const config = require('../../utils/config.js');
const auth = require('../../utils/auth.js');

Page({
  data: {
    statusBarHeight: app.globalData.statusBarHeight,
    babyName: '宝宝',
    userName:'用户',
    userId:0,
    
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getUserInfoFromServer();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: this.data.userName + '邀您一起记录' + this.data.babyName +'的成长',
      path: '/pages/invitopen/invitopen?userName=' + this.data.userName + '&invitUserId=' + this.data.userId + '&babyId=' + this.data.babyId+'&babyName='+this.data.babyName,
    }
  },
  goBack: function () {
    wx.navigateBack({})
  },
  
  getUserInfoFromServer: function () {
    let that = this
    config.postApi(config.getUserInfo, {}).then((res) => {
      that.setData({
        babyId: res.babyInfo.babyId,
        babyName: res.babyInfo.name,
        userName: res.invitInfo.myNickname,
        userId: res.invitInfo.myId,
      })
    }).catch((res) => {
      console.log(res)
    })
  },
  
})