// pages/today/today.js
const app = getApp()
const config = require('../../utils/config.js');
const auth = require('../../utils/auth.js');

Page({
  data: {
    statusBarHeight: app.globalData.statusBarHeight,
    babyNickname: '宝宝',
    optionActive: 1,
    selectColor1: 'color: #FF9DCA;',
    selectColor2: '',
    selectedTime: '',
    selectedRelationType:'妈妈',
    relationType: ['妈妈', '爸爸', '奶奶', '爷爷', '姥姥', '姥爷', '叔叔', '阿姨', '婶婶', '舅舅', '舅妈'],
    showEidt: false,
    /*模块 begin */
    weinai: 1,
    niaobu: 1,
    shuijiao: 1,
    xinai: 1,
    //辅食默认是关闭的
    fushi: 2,
    /*模块 end */
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let date = new Date()
    var str = date.getFullYear() + '-';
    str += (date.getMonth() + 1) + '-';
    str += date.getDate();
    this.setData({
      selectedTime: str
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.checkModel();
    this.getUserInfoFromServer();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  goBack: function () {
    wx.navigateBack({})
  },
  selectOption: function (e) {
    this.setData({
      showEidt: true
    })
    let option = e.currentTarget.dataset.option;
    if (option == 1) {
      this.setData({
        optionActive: 1,
        selectColor1: 'color: #FF9DCA;',
        selectColor2: '',
      })
    } else {
      this.setData({
        optionActive: 2,
        selectColor1: '',
        selectColor2: 'color: #FF9DCA;',
      })
    }
  },
  listenBabyNickname: function (e) {
    var val = e.detail.value;
    this.setData({
      babyNickname: val,
      showEidt: true,
    });
  },
  bindTimeChange(e) {
    this.setData({
      selectedTime: e.detail.value,
      showEidt: true,
    })
  },
  bindRelationChange(e) {
    this.setData({
      selectedRelationType: this.data.relationType[e.detail.value],
      showEidt: true,
    })
  },
  getUserInfoFromServer: function () {
    let that = this
    config.postApi(config.getUserInfo, {}).then((res) => {
      if (res.babyInfo.gender==2) {
        that.setData({
          optionActive: 2,
          selectColor1: '',
          selectColor2: 'color: #FF9DCA;',
        })
      } else {
        that.setData({
          optionActive: 1,
          selectColor1: 'color: #FF9DCA;',
          selectColor2: '',
        })
      }
      
      that.setData({
        babyInfo: res.babyInfo,
        invitInfo: res.invitInfo,
        babyNickname: res.babyInfo.name,
        selectedTime: res.babyInfo.birthday,
        selectedRelationType:res.babyInfo.relationType,
      })
    }).catch((res) => {
      console.log(res)
    })
  },
  editUserInfoToServer: function () {
    let that = this
    let param = {
      birthday: this.data.selectedTime,
      babyNickname: this.data.babyNickname,
      gender: this.data.optionActive,
      relationType: this.data.selectedRelationType,
    }
    config.postApi(config.editUserInfo, param).then((res) => {
      wx.showToast({
        title: "修改成功",
        icon: 'none',
        image: '',
        duration: 1500,
        mask: false,
      });
      that.setData({
        showEidt:false
      })
    }).catch((res) => {
      console.log(res)
    })
  },
  gotoInvit: function () {
    wx.navigateTo({
      url: '/pages/invit/invit'
    })
  },
  changeMd:function (e) {
    let id = e.currentTarget.dataset.id
    let val = e.currentTarget.dataset.val
    if (val==1) {
      val = 2;
      wx.showToast({
        title: "已隐藏",
        icon: 'none',
        image: '',
        duration: 1500,
        mask: false,
      });
    } else {
      val =1;
      wx.showToast({
        title: "已打开",
        icon: 'none',
        image: '',
        duration: 1500,
        mask: false,
      });
    }
    this.setData({
      [id]:val
    })
    wx.setStorageSync("md_"+id, val);
  },
  checkModel:function () {
    let weinai = wx.getStorageSync('md_weinai');
    console.log("today weinai=" + weinai)
    if (weinai == '') {
      //第一次进入，需要初始化
      console.log("第一次进入，需要初始化")
      wx.setStorageSync("md_weinai", 1);
      wx.setStorageSync("md_shuijiao", 1);
      wx.setStorageSync("md_niaobu", 1);
      wx.setStorageSync("md_xinai", 1);
      wx.setStorageSync("md_fushi", 2);
      return;
    }
    this.setData({ weinai: weinai })
    this.setData({ niaobu: wx.getStorageSync('md_niaobu') })
    this.setData({ shuijiao: wx.getStorageSync('md_shuijiao') })
    this.setData({ xinai: wx.getStorageSync('md_xinai') })
    this.setData({ fushi: wx.getStorageSync('md_fushi') })
  }
})