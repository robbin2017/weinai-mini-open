// pages/today/today.js
const app = getApp()
const config = require('../../utils/config.js');
const auth = require('../../utils/auth.js');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    statusBarHeight: app.globalData.statusBarHeight,
    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    recordList: [],
    /* 滚动效果 begin */
    svPoint: 1,
    svSize: 3,
    /* 滚动效果 end */
    babyInfo: {
      gender: 1,
      name: '宝宝'
    },
    invitInfo: {},
    rollList: [],
    showRoll: false,
    isEmpty:false,
    tips:'宝宝喝完奶记得拍嗝哦',
    /*模块 begin */
    weinai:1,
    niaobu:1,
    shuijiao: 1,
    xinai:1,
    //辅食默认是关闭的
    fushi: 2,
    /*模块 end */
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    let sessionId = auth.checkDoLogin();
    this.setData({
      sessionId: sessionId,
    })
    let userInfo = wx.getStorageSync('userInfo');
    if (userInfo) {
      this.setData({
        userInfo: userInfo,
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    this.checkModel()
    let sessionId = wx.getStorageSync('sessionId');
    this.setData({
      sessionId: sessionId,
    })
    if (this.data.sessionId) {
      this.getTodayRecordList();
      this.getUserInfoFromServer();
    }
    this.showRoll();
  },

  getTodayRecordList: function() {
    let that = this;
    config.postApi(config.getTodayRecord, {}).then((res) => {
      let list = res.list
      that.setData({
        recordList: list,
        isEmpty: res.isEmpty,
      })
    }).catch((res) => {
      console.log(res)
    })
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },

  getUserInfo: function(e) {
    let that = this
    console.log(e)
    let userInfo = e.detail.userInfo
    userInfo.isLogin = true
    wx.setStorageSync('userInfo', userInfo);
    this.setData({
      userInfo: userInfo,
    })
    wx.login({
      success: res => {
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
        let param = {
          sessionId: that.data.sessionId,
          code: res.code,
        }
        console.log('微信登录请求：' + param)
        config.postApi(config.wxLogin, param).then((res) => {
          let newSessionId = res.sessionId
          wx.setStorageSync('sessionId', newSessionId);
          that.setData({
            sessionId: newSessionId,
          })
          //保存用户的信息
          config.postApi(config.addUserWxInfo, userInfo).then((res) => {
            //保存成功
            that.getTodayRecordList();
            that.getUserInfoFromServer();
          }).catch((res) => {
            console.log(res)
          })
        }).catch((res) => {
          console.log(res)
        })
      }
    })

  },
  addWeinai: function() {
    wx.navigateTo({
      url: '/pages/weinai/weinai'
    })
  },
  addNiaobu: function() {
    wx.navigateTo({
      url: '/pages/niaobu/niaobu'
    })
  },
  addShuijiao: function() {
    wx.navigateTo({
      url: '/pages/shuijiao/shuijiao'
    })
  },
  addFushi: function() {
    wx.navigateTo({
      url: '/pages/fushi/fushi'
    })
  },
  addXinai: function() {
    wx.navigateTo({
      url: '/pages/xinai/xinai'
    })
  },
  gotoRecord: function(e) {
    let id = e.currentTarget.dataset.id;
    let recordType = e.currentTarget.dataset.recordtype;
    if (recordType == 1) {
      wx.navigateTo({
        url: '/pages/weinai/weinai?recordId=' + id
      })
    } else if (recordType == 2) {
      wx.navigateTo({
        url: '/pages/niaobu/niaobu?recordId=' + id
      })
    } else if (recordType == 3) {
      wx.navigateTo({
        url: '/pages/shuijiao/shuijiao?recordId=' + id
      })
    } else if (recordType == 4) {
      wx.navigateTo({
        url: '/pages/fushi/fushi?recordId=' + id
      })
    } else if (recordType == 5) {
      wx.navigateTo({
        url: '/pages/xinai/xinai?recordId=' + id
      })
    }
  },
  delRecord: function(e) {
    let that = this;
    let id = e.currentTarget.dataset.id;
    let recordType = e.currentTarget.dataset.recordtype;
    wx.showModal({
      title: '',
      content: '是否删除此记录?',
      showCancel: true,
      confirmText: '删除',
      confirmColor: '#f5222d',
      success(res) {
        if (res.confirm) {
          config.postApi(config.delRecord, {
            recordId: id
          }).then((res) => {
            //删除成功
            wx.showToast({
              title: "删除成功",
              icon: 'none',
              image: '',
              duration: 1500,
              mask: false,
            });
            that.getTodayRecordList();
          }).catch((res) => {
            console.log(res)
          })
        }
      }
    })
  },
  gotoMyinfo: function() {
    if (this.data.userInfo.isLogin) {
      wx.navigateTo({
        url: '/pages/myinfo/myinfo'
      })
    } else {
      wx.showToast({
        title: "请先登录",
        icon: 'none',
        image: '',
        duration: 1500,
        mask: false,
      });
    }
  },
  gotoInvit: function() {
    if (this.data.userInfo.isLogin) {
      wx.navigateTo({
        url: '/pages/invit/invit'
      })
    } else {
      wx.showToast({
        title: "请先登录",
        icon: 'none',
        image: '',
        duration: 1500,
        mask: false,
      });
    }

  },
  getUserInfoFromServer: function() {
    let that = this
    config.postApi(config.getUserInfo, {}).then((res) => {
      that.setData({
        babyInfo: res.babyInfo,
        invitInfo: res.invitInfo,
        tips: res.tips,
      })
    }).catch((res) => {
      console.log(res)
    })
  },
  showRoll: function() {
    let that = this;
    config.postApi(config.getRecordRollList, {}).then((res) => {
      let preShowRoll = that.data.showRoll;
      that.setData({
        rollList: res.recordList,
        showRoll: true,
        svSize: res.size,
      })
      //第一次启动滚轮
      if (!preShowRoll) {
        //滚动数据
        setInterval(function () {
          let svPoint = that.data.svPoint;
          let svSize = that.data.svSize;
          svPoint++;
          if (svPoint > svSize) {
            svPoint = 1;
          }
          that.setData({
            svPoint: svPoint,
          })
        }, 3000)
      }
      
    }).catch((res) => {
      console.log(res)
    })
  },
  checkModel:function() {
    let weinai = wx.getStorageSync('md_weinai');
    console.log("today weinai=" + weinai)
    if (weinai=='') {
      //第一次进入，需要初始化
      console.log("第一次进入，需要初始化")
      wx.setStorageSync("md_weinai", 1);
      wx.setStorageSync("md_shuijiao", 1);
      wx.setStorageSync("md_niaobu", 1);
      wx.setStorageSync("md_xinai", 1);
      wx.setStorageSync("md_fushi", 2);
      return;
    }
    this.setData({ weinai: weinai })
    this.setData({ niaobu: wx.getStorageSync('md_niaobu') })
    this.setData({ shuijiao: wx.getStorageSync('md_shuijiao') })
    this.setData({ xinai: wx.getStorageSync('md_xinai') })
    this.setData({ fushi: wx.getStorageSync('md_fushi') })
  }
})