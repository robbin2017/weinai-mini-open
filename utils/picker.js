function pickerTodayWeight(num){
  var pickerWeightArray = [];

  //下 10
  for(var i=100 ;i>0;i--){
    var upobj = {};
    upobj.value = num - (100*i);
    upobj.key = (num - (100*i))/1000+' kg';
    pickerWeightArray.push(upobj);
  }
  var currentobj = {};
  currentobj.value = num;
  currentobj.key = num/1000+' kg'
  pickerWeightArray.push(currentobj);
  //上 10
  for(var i=1 ;i<=100;i++){
    var downobj = {};
    downobj.value = num + (100*i);
    downobj.key = (num + (100*i))/1000 +' kg'
    pickerWeightArray.push(downobj);
  }
  var pickerobj = {};
  pickerobj.pickerWeightArray = pickerWeightArray;
  pickerobj.pickerWeightIndex = 100;

  return pickerobj;
}

function pickerDate(){
  var pickerDateArray = ['今天','昨天'];
  let begin = new Date()
  //昨天
  begin.setTime(begin.getTime() - 24 * 60 * 60 * 1000);
  for(var i=0;i<=100;i++){
    begin.setTime(begin.getTime() - 24 * 60 * 60 * 1000);
    var str = begin.getFullYear()+'-';
    str += (begin.getMonth()+1) + '-';
    str += begin.getDate();
    pickerDateArray.push(str)
  }
  return pickerDateArray;
}
function pickerEducation() {
  var pickerEducationArray = ['初中','高中','专科','本科','硕士','博士'];
  return pickerEducationArray;
}
module.exports = {
  pickerTodayWeight: pickerTodayWeight,
  pickerDate: pickerDate,
  pickerEducation: pickerEducation,
}
