const config = require('config.js');

function checkDoLogin() {
  // 判断是否登录
  let sessionId = wx.getStorageSync('sessionId');
  if (!sessionId) {
    //新用户
    config.postApi(config.getNewSession, {}).then((res) => {
      sessionId = res.sessionId
      wx.setStorageSync('sessionId', sessionId);
      console.log('return sessionId='+sessionId)
    }).catch((res) => {
      console.log(res)
    })
    //存入游客信息
    let userInfo = {
      avatarUrl: '../../images/unlogin.jpeg',
      nickName: '未登录',
      isLogin: false,
    }
    wx.setStorageSync('userInfo', userInfo)
  } else {
    //不是新用户
  }
  return sessionId;
}

module.exports = {
  checkDoLogin: checkDoLogin,
}