const appid = '【TODO 这里是小程序的appid】';
const requestKey = '【TODO 这里是和后端约定的一个key】';
// 服务器域名
const baseUrl = '【TODO 这里是域名】';

//新用户
const getNewSession = baseUrl + '/auth/getNewSession';
//微信登录
const wxLogin = baseUrl + '/auth/wxLogin';
//更新微信信息
const addUserWxInfo = baseUrl + '/auth/addUserWxInfo';
//添加记录
const addRecord = baseUrl + '/record/addRecord';
//删除记录
const delRecord = baseUrl + '/record/delRecord';
//今日记录
const getTodayRecord = baseUrl + '/record/getTodayRecord';
//今日记录
const getOneDayRecord = baseUrl + '/record/getOneDayRecord';
//获取单条记录
const getRecord = baseUrl + '/record/getRecord';
//获取记录列表
const getRecordList = baseUrl + '/record/getRecordList';
//获取用户信息
const getUserInfo = baseUrl + '/user/getUserInfo';
//修改用户信息
const editUserInfo = baseUrl + '/user/editUserInfo';
//接收邀请
const acceptInvit = baseUrl + '/user/acceptInvit';
//接收邀请
const getRecordRollList = baseUrl + '/record/getRecordRollList';



/**用Promise解决wx.request异步请求带来的问题
 * method:servlet的函数名,如query、insert
 * parameter:
 */
function postApi(method, parameter) {
  let sessionId = wx.getStorageSync('sessionId');
  console.log('postApi session='+sessionId)
  parameter.sessionId = sessionId;
  parameter.appid = appid;
  parameter.requestKey = requestKey;
  return new Promise(function (resolve, reject) {
    wx.request({
      url: method,
      data: parameter,
      header: {
        'content-type': 'application/json',
      },
      method: 'POST',
      dataType: 'json',
      responseType: 'text',
      success: (result) => {
        if (result.data.returncode == 200) {
          let data = result.data.data
          resolve(data);
        } else {
          wx.showToast({
            title: result.data.message,
            icon: 'none',
            image: '',
            duration: 2000,
            mask: false,
          });
          setTimeout(function () {
            reject(result)
          }, 2000)
        }
      },
      fail: () => { reject("系统异常，请重试！") },
      complete: () => { }
    });
  })
}

module.exports = {
  postApi: postApi,
  requestKey: requestKey,
  getNewSession: getNewSession,
  wxLogin: wxLogin,
  addRecord: addRecord,
  getTodayRecord: getTodayRecord,
  getOneDayRecord: getOneDayRecord,
  addUserWxInfo: addUserWxInfo,
  getRecord: getRecord,
  getRecordList: getRecordList,
  delRecord: delRecord,
  getUserInfo: getUserInfo,
  editUserInfo: editUserInfo,
  acceptInvit: acceptInvit,
  getRecordRollList: getRecordRollList,
};