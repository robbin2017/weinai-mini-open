//app.js
App({
  onLaunch: function () {
    // 展示本地存储能力
    var logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)
  },
  //自定义顶栏高度
  globalData: {
    statusBarHeight: wx.getSystemInfoSync()['statusBarHeight'] + 6
  },
  
})